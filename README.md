# Home Automation with Wireless Control

This project is meant to demonstrate how different tools, such as Android phones with wireless capabilities, OrangePi boards with LAN and wireless connectivity, and home PCs can fall under one consolidated home automation system. As a proof-of-concept, the primary automated task will be to log into our college network, triggered by an SMS from any other phone. Once logged in, it will provide internet access for an array of devices that are connected to the same private network.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

* Windows/Linux PC with SSH capability
* OrangePi (or RaspberryPi)
* Router
* Repeater
* MicroSD card
* Rooted Android phone

### Installing

The first phase is to set up the OrangePi board.

First download the legacy kernel (stable) of the Armbian OS for OrangePi Zero from 
[here](https://www.armbian.com/orange-pi-zero/ "Armbian for OrangePi Zero").

You need to burn this onto the microSD card. This is easily done with the cross-platform tool, [Etcher](https://etcher.io/ "Etcher").

After flashing the OS onto the microSD card, insert it into a slot on the OrangePi and power the board. It will take about 35 seconds to fully power on.

Connect this to the router via Ethernet cable. Make sure the PC is connected to the same router as well.

Once this is done, find the IP address assigned to the OrangePi board. This can be done via the `nmap` command line tool, or by going to the router's home login page and checking all assigned IP addresses.

After this, connect to the root user on the OrangePi through SSH.

```
ssh root@<orangepi-ip-address>
```
The default password is `1234`. It will force you to change the root password, and to create a new user account.

Mechanize and BeautifulSoup are python modules which allow for accessing web pages and interacting with them. We will need them to perform the actual logging in and out of the college intranet. Install the following packages:
```
sudo -H apt-get install python-lxml
sudo -H apt-get install python-pip
sudo -H pip install mechanize
sudo -H pip install bs4
sudo -H pip install lxml
```

After this, logout of the SSH session. Navigate to the project directory on the local PC, and upload the files in the `credential-swiper` directory (of this project) to the `/home/` directory of the OrangePi board.

```
scp config.py <username>@<orangepi-ip-address>:~/
scp driver.py <username>@<orangepi-ip-address>:~/
scp login.py <username>@<orangepi-ip-address>:~/
scp logout.py <username>@<orangepi-ip-address>:~/
scp cardmanager.py <username>@<orangepi-ip-address>:~/
scp utils.py <username>@<orangepi-ip-address>:~/
```

### Creating a Tasker profile on an Android phone.
Tasker is an application for Android which performs tasks (sets of actions) based on contexts (application, time, date, location, event, gesture) in user-defined profiles or in clickable or timer home screen widgets. This simple concept profoundly extends your control of your Android device and it's capabilities, without the need for 'root' or a special home screen.

Here, a Tasker profile needs to be created that will be capable of detecting commands in messages, and forwarding them through TCP to the OrangePi.

Download and install the [Tasker application](https://play.google.com/store/apps/details?id=net.dinglisch.android.taskerm) from the Google Play Store (or any other reliable source). This is required to read SMS and detect specific commands send from remote users.
Download and install the [Send/Expect Tasker plugin](https://play.google.com/store/apps/details?id=com.asif.plugin.sendexpect) from the Google Play Store (or any other reliable source). This is required to send specific commands from the Android phone to the OrangePi board over TCP.

Download the `Microtrix.prj.xml` file (in the /android-tasker/ folder of this repository), and save it to your android phone.
Then import this project into your Tasker application. A new project called "Microtrix" should appear.

This phone also requires a SIM card that is capable of receiving SMS messages.

Finally, connect the phone via WiFi to the router that the OrangePi is connected to. Effectively, the phone and the OrangePi must be on the same local area network.

## Running the tests

The testing mechanisms for the individual modules of this architecture are listed here.


### <a name="accessCardInstructions"></a>Creating an access card with login credentials.

Before logging into the internet connectivity portal (in the case of Manipal University, the portal is owned and maintained by I-ON Web Axis), an access card with accurate login credentials needs to be created. The `cardmanager.py` script is capable of doing this. Run the following command to create (or update) the access card:

```
python cardmanager.py "newUsername" "newPassword"
```
or
```
python cardmanager.py "newUsername" "newPassword" "retryInterval"
```

The retry interval can be specified for manually defining the period of time that elapses between consecutive internet connectivity checks.

### <a name="swiperInstructions"></a>Logging in and out using the credential swiper.

The credential swiper is designed to keep attempting to log in after regular time intervals, so as to prevent internet connectivity loss for devices connected to the private network if a logout occurs automatically. 

First, launch a new `screen` to execute the driver script. This allows for the terminal on the local machine to be closed without killing the process on the OrangePi board.

```
screen
```

Before starting the login driver script, an access card with correct credentials must be created. Follow the instructions in the [previous section](#accessCardInstructions) for details on how to do this.

Then, execute the driver script. 
```
python driver.py <controller_ip_address>
```

Here is a sample output:
```
soorya@orangepizero:~/home-automate/credential-swiper$ python driver.py 10.0.0.3
Waiting for connection
Waiting for connection
Waiting for connection
Waiting for connection
Waiting for connection
Waiting for connection
```

At this point, the OrangePi is waiting for commands to arrive via TCP to the aforementioned IP address. 

There are three presently defined commands that can be utilized:
* login
* logout
* shutdown

#### Command: login
The login command starts a persistent login procedure, and keep infinitely checking for internet connectivity (to ensure that an accidental or unexplained logout does not affect any devices attached to the local network). If an active internet connection is detected, then no further action is taken for a predefined retry interval. However, if there is no active internet connection detected, it will perform a login operation. This will stop only when there is an error:

There are three ways to deliver a login message.
1. From another phone, send an SMS to the pre-configured phone with the SMS text body as `microtrix:login`.
2. Navigate to the imported Tasker project (named Microtrix), go to the Tasks view, long-press on the "Quick Login" task, and execute it (option on the top right).
3. Download the `tcpclient.py` script from this repository, and execute the following shell command: `python tcpclient.py <controller_ip_address>`. Then enter `login` when asked for the message to send.

If successful, the output will resemble the following sample output:
```
soorya@orangepizero:~/home-automate/credential-swiper$ python driver.py 10.0.0.3
Waiting for connection
Waiting for connection
Waiting for connection
Waiting for connection
Received:  login
Sent:  login
Attempting login for <140905116> at time: Sun Oct  8 10:15:50 2017
Microtrix is online (persist): Sun Oct  8 10:15:51 2017
Waiting for connection
Waiting for connection
Waiting for connection
Waiting for connection
Waiting for connection
Waiting for connection
Attempting login for <140905116> at time: Sun Oct  8 10:16:03 2017
Microtrix is online (persist): Sun Oct  8 10:16:03 2017
Waiting for connection
Waiting for connection
Waiting for connection
Waiting for connection
Waiting for connection
Waiting for connection
Attempting login for <140905116> at time: Sun Oct  8 10:16:15 2017
Waiting for connection
Microtrix is online (persist): Sun Oct  8 10:16:16 2017
Waiting for connection
Waiting for connection
Waiting for connection
Waiting for connection
Waiting for connection
```

#### Command: logout

To trigger a graceful exit by logging out of the internet portal, perform any one of the following actions:
1. From another phone, send an SMS to the pre-configured phone with the SMS text body as `microtrix:logout`.
2. Navigate to the imported Tasker project (named Microtrix), go to the Tasks view, long-press on the "Quick Logout" task, and execute it (option on the top right).
3. Download the `tcpclient.py` script from this repository, and execute the following shell command: `python tcpclient.py <controller_ip_address>`. Then enter `logout` when asked for the message to send.

If successful, the output will resemble the following sample output:
```
Waiting for connection
Attempting login for <140905116> at time: Sun Oct  8 11:01:07 2017
Microtrix is online (persist): Sun Oct  8 11:01:07 2017
Waiting for connection
Waiting for connection
Waiting for connection
Received:  logout
Sent:  logout
Waiting for connection
Successfully logged out: Sun Oct  8 11:01:20 2017
Waiting for connection
Waiting for connection
Waiting for connection
Waiting for connection
Waiting for connection
Waiting for connection
Waiting for connection
```

The script is still running, and can receive further commands.

#### Command: shutdown

To command the script to gracefully shutdown and perform all necessary housekeeping procedures, perform any one of the following actions:
1. From another phone, send an SMS to the pre-configured phone with the SMS text body as `microtrix:shutdown`.
2. Navigate to the imported Tasker project (named Microtrix), go to the Tasks view, long-press on the "Quick Shutdown" task, and execute it (option on the top right).
3. Open the running script on the SSH terminal, and enter `Ctrl + c`.
4. Download the `tcpclient.py` script from this repository, and execute the following shell command: `python tcpclient.py <controller_ip_address>`. Then enter `shutdown` when asked for the message to send.

If the `shutdown` command is successfully executed, the output will resemble the following sample output:
```
Attempting login for <140905116> at time: Sun Oct  8 11:13:54 2017
Microtrix is online (persist): Sun Oct  8 11:13:55 2017
Waiting for connection
Waiting for connection
Received:  shutdown
Sent:  shutdown
Driver function ending
INFO:CredentialSwiper:Driver function ending
soorya@orangepizero:~/home-automate/credential-swiper$ 
```

Upon successful completion of the `shutdown` command, the script will no longer be active and listening for further commands. If it is required to perform a subsequent login, the script needs to be manually restarted (as described in the [above section](#swiperInstructions)).

### Utility functions

Here are some of the other features that can be capitalized upon based on the requirements at hand.

#### Running the login script in the background.
To exit the SSH terminal from the local machine without killing the process on the OrangePi board, detach the screen running the process, and return to the normal SSH terminal by keying in `Ctrl + a, d`. Then input `exit` to close the SSH connection.

It is also possible to access the screen of an already running `driver.py` script after logging into through SSH. Input:
```
screen -ls
```
An example output is:
```
soorya@orangepizero:~$ screen -ls
There is a screen on:
	7866.pts-2.orangepizero	(08/24/2017 11:59:14 PM)	(Detached)
1 Socket in /var/run/screen/S-soorya.
```
This will give a list of screen IDs that are currently active.

To reconnect to the screen running the `driver.py` script, type in:
```
screen -r 7866.pts-2.orangepizero
```
This will return to the screen with the (limited) output of the `driver.py` script that is currently executing.

#### Access card usage history
The access card also preserves the history of all previously used credentials. To view the access card (and it's history), run the command `cat card.csv`. The last entry corresponds to the currently active credentials.

## Deployment

(Need to add additional notes about how to deploy this on a live system.)

### Network Architecture
* Connect OrangePi board to router via Ethernet.
* Connect phone to router via WiFi.
* Connect PC to router via WiFi or Ethernet.
* Configure repeater to extend wireless signal of college WiFi (where intranet login portal is accessible).
* Connect router (input) to repeater (output) via Ethernet.


## Developers

* **Soorya Annadurai**
* **Asparsh Kumar**

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Acknowledgments

* Mrs. Mamatha Balachandra, our Wireless Networks professor who allowed us to carry out this project for the course of the semester.
* Siddharth Sahay, who lent us the OrangePi board and helped with the initial OrangePi OS configuration!

