import mechanize
from bs4 import BeautifulSoup

# ION_URL = 'https://mahe3.dvois.com/24online/webpages/client.jsp'
ION_URL = 'https://1.186.23.39/24online/webpages/client.jsp'


def clean_response(browser, original_response):
	soup = BeautifulSoup(original_response.read(), "lxml")
	for div in soup.findAll('select'):
		div.extract()
	original_response.set_data(str(soup))
	browser.set_response(original_response)
	soup = BeautifulSoup(browser.response().read(), "lxml")
	html = str(soup)
	cleaned_response = mechanize.make_response(html, [("Content-Type", "text/html")], browser.geturl(), 200, "OK")
	return cleaned_response


class LoginError(Exception):
	def __init__(self, msg, timestamp, response):
		Exception.__init__(self, msg)
		self.timestamp = timestamp
		self.response = response


class CredentialError(LoginError):
	def __init__(self, timestamp, response):
		LoginError.__init__(self, "Incorrect credentials", timestamp, response)

	# LoginError.__init__(self=self, msg="Incorrect credentials", timestamp=timestamp, response=response)
	# super(CredentialError, self).__init__(msg="Incorrect credentials", timestamp=timestamp, response=response)


class MultiuserError(LoginError):
	def __init__(self, timestamp, response):
		# LoginError.__init__(self=self, msg="Different username on the same IP", timestamp=timestamp, response=response)
		LoginError.__init__(self, "Different username on the same IP", timestamp, response)

class QuotaError(LoginError):
	def __init__(self, timestamp, response):
		# LoginError.__init__(self=self, msg="Data quota exhausted", timestamp=timestamp, response=response)
		LoginError.__init__(self, "Data quota exhausted", timestamp, response)


class StrangeError(LoginError):
	def __init__(self, timestamp, response):
		# LoginError.__init__(self=self, msg="Strange scenario encountered", timestamp=timestamp, response=response)
		LoginError.__init__(self, "Strange scenario encountered", timestamp, response)
