import Queue
import csv
import logging.handlers
import os
import select
import socket
import sys
import threading
import time
import urllib2

import cardmanager
import login
import logout
import utils

LOG_FILENAME = 'credential-swiper.log'


def is_internet_connected():
	try:
		header = {"pragma": "no-cache"}  # Tells the server to send fresh copy
		req = urllib2.Request("http://testingmywebapp.azurewebsites.net/", headers=header)
		response = urllib2.urlopen(req)
		a = response.read()
		if a.strip() == "Hi Microtrix":
			return True
		else:
			return False
	except urllib2.URLError:
		return False
	except Exception:
		return False


def persistent_login(username, password, retry_interval, logger, trigger_queue, notify_queue):
	logger.info("Starting persistent login for username: <" + username + ">")
	login_lifeline = True
	login_permission = False
	try:
		if username is None or password is None or retry_interval is None:
			raise AttributeError("Invalid arguments")
		run_flag = True
		while run_flag:
			try:
				trigger_queue_item = trigger_queue.get(timeout=2)
				trigger_queue.task_done()
				if trigger_queue_item == "STOP":
					run_flag = False
					notify_queue.put("SHUTDOWN")
					continue
				elif trigger_queue_item == "LOGOUT":
					# TODO Notify logout with acknowledgement
					try:
						logout.logout()
					except urllib2.URLError as err:
						logger.error("Logout failed! " + str(err.reason))
						pass
					login_permission = False
				elif trigger_queue_item == "LOGIN":
					# TODO Notify login with acknowledgement
					login_permission = True
			except Queue.Empty:
				pass

			if login_permission is True:
				print("Attempting login for <" + username + "> at time: " + time.ctime())
				try:
					if is_internet_connected() is True:
						logger.info("Internet is tested and connected")
						print("Microtrix is online (persist): " + time.ctime())
					else:
						logger.info("Internet connectivity test failed")
						login.login(username=username, password=password)
						login_lifeline = True
						print("Microtrix is online (re-login): " + time.ctime())
					time.sleep(retry_interval)
				except urllib2.URLError as url_error:
					logger.info("Encountered urlopen error: Name or service not known")
					logger.error(str(url_error.reason))
				except utils.StrangeError:
					pass
				except utils.LoginError as login_error:
					logger.error(login_error.message)
					logger.debug(login_error.response)
					if login_lifeline is True:
						login_lifeline = False
						logger.info("Used a login lifeline. Retrying...")
					else:
						raise login_error

	except (KeyboardInterrupt, SystemExit):
		try:
			logger.info("Logging out for username: <" + username + ">")
			logout.logout()
			logger.info("Logged out successfully")
		except urllib2.URLError as url_error:
			logger.info("Failed logout procedure due to urlopen error")
			logger.error(url_error.reason)
		logger.info("Exiting persistent login script")


def network_watcher(trigger_queue, notify_queue, ip_address, logger):
	try:
		# A simple echo server
		server_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		server_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		port = 12345
		ip_addr = ip_address
		server_address = (ip_addr, port)
		server_sock.bind(server_address)
		server_sock.listen(5)
		run_flag = True
		read_list = [server_sock]
		while run_flag:
			trigger_queue_item = None
			socket_accept_wait_time = 2
			# TODO: What will happen if I make this infinity?
			readable, writable, erred = select.select(read_list, [], [], socket_accept_wait_time)
			try:
				trigger_queue_item = trigger_queue.get(block=False)
				trigger_queue.task_done()
				if trigger_queue_item == "STOP":
					notify_queue.put("SHUTDOWN")
					# print "Put SHUTDOWN in network notify queue"
					run_flag = False
					continue
			except Queue.Empty:
				trigger_queue_item = None
			print "Waiting for connection"
			for s in readable:
				if s is server_sock:
					client_sock, client_addr = server_sock.accept()
					read_list.append(client_sock)
				else:
					# data = client_sock.recv(16)
					data = s.recv(16)
					print "Received: ", data
					logger.info("Received command: " + data)
					if data == "login":
						notify_queue.put("LOGIN")
					# TODO: Get login confirmation
					elif data == "logout":
						notify_queue.put("LOGOUT")
					elif data == "shutdown":
						run_flag = False
						notify_queue.put("SHUTDOWN")
					# client_sock.sendall(data)
					s.sendall(data)
					print "Sent: ", data
					# client_sock.close()
					s.close()
					# read_list.remove(client_sock)
					read_list.remove(s)
	except (KeyboardInterrupt, SystemExit):
		notify_queue.put("KILLED")
		print "Killing the network module"
		pass


def driver(username, password, retry_interval, ip_address, logger):
	# TODO Add help flag
	# TODO Add option for force-start login (put LOGIN in swiper_trigger_queue)
	swiper_trigger_queue = Queue.Queue()
	swiper_notify_queue = Queue.Queue()
	network_trigger_queue = Queue.Queue()
	network_notify_queue = Queue.Queue()
	driver_run_flag = True

	thread_swiper = threading.Thread(
		target=persistent_login,
		args=(username, password, retry_interval, logger, swiper_trigger_queue, swiper_notify_queue)
	)
	thread_network = threading.Thread(
		target=network_watcher,
		args=(network_trigger_queue, network_notify_queue, ip_address, logger)
	)
	thread_swiper.start()
	thread_network.start()
	while driver_run_flag is True:
		try:
			network_queue_item = network_notify_queue.get(timeout=retry_interval)
			network_notify_queue.task_done()
			if network_queue_item == "SHUTDOWN":
				swiper_trigger_queue.put("STOP")
				swiper_queue_item = swiper_notify_queue.get(block=True)
				swiper_notify_queue.task_done()
				assert swiper_queue_item == "SHUTDOWN"
				driver_run_flag = False
			elif network_queue_item == "LOGOUT":
				swiper_trigger_queue.put("LOGOUT")
			elif network_queue_item == "LOGIN":
				swiper_trigger_queue.put("LOGIN")
		except Queue.Empty:
			pass
		except (KeyboardInterrupt, SystemExit):
			network_trigger_queue.put("STOP")
			swiper_trigger_queue.put("STOP")
			print "Waiting for stop notification from swiper thread"
			swiper_queue_item = swiper_notify_queue.get(block=True)
			swiper_notify_queue.task_done()
			print "Waiting for stop notification from network thread"
			while True:
				try:
					# TODO Fix this! This is a bodge. Ideally, it should just be block=True
					# network_queue_item = network_notify_queue.get(block=True)
					network_queue_item = network_notify_queue.get(block=False, timeout=2)
					break
				except Queue.Empty:
					pass
			print "Received close confirmations from swiper and network notification queues!"
			logger.info("Received close confirmations from swiper and network notification queues!")
			network_notify_queue.task_done()
			assert swiper_queue_item == "SHUTDOWN"
			assert network_queue_item == "SHUTDOWN" or network_queue_item == "KILLED"

			# TODO Force-kill all queues and threads
			driver_run_flag = False

	logging.debug("Waiting for network thread to join")
	thread_network.join()
	logging.debug("Waiting for swiper thread to join")
	thread_swiper.join()
	logging.debug("Waiting for network_notify_queue to join")
	network_notify_queue.join()
	logging.debug("Waiting for network_trigger_queue to join")
	network_trigger_queue.join()
	logging.debug("Waiting for swiper_notify_queue to join")
	swiper_notify_queue.join()
	logging.debug("Waiting for swiper_trigger_queue to join")
	swiper_trigger_queue.join()
	print "Driver function ending"
	logger.info("Driver function ending")


if __name__ == "__main__":
	# Set up a specific logger with our desired output level
	my_logger = logging.getLogger('CredentialSwiper')
	my_logger.setLevel(logging.DEBUG)

	# Add the log message handler to the logger
	handler = logging.handlers.RotatingFileHandler(LOG_FILENAME, maxBytes=20000, backupCount=10)
	handler.setFormatter(logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s"))
	my_logger.addHandler(handler)

	# Check if log exists and should therefore be rolled
	needRoll = os.path.isfile(LOG_FILENAME)
	if needRoll:
		my_logger.info('\n---------\nLog closed on %s.\n---------\n' % time.asctime())
		# Roll over on application start
		my_logger.handlers[0].doRollover()

	if len(sys.argv) == 2:
		try:
			username = None
			password = None
			interval = None
			with open(cardmanager.CARD_PATH) as f_obj:
				reader = csv.DictReader(f_obj, delimiter=',')
				for line in reader:
					username = line[cardmanager.CARD_HEADERS['username']]
					password = line[cardmanager.CARD_HEADERS['password']]
					interval = line[cardmanager.CARD_HEADERS['interval']]
			driver(
				username=username, password=password, retry_interval=int(interval), ip_address=sys.argv[1],
				logger=my_logger
			)
		except (IOError, KeyError) as e:
			raise IOError("Invalid or nonexistent card file!")
	else:
		print(
			"Correct usage: " + "\n" + sys.argv[0] + " <device_ip_address>"
		)
