import sys
from threading import Thread

from include.Controller.Controller import Controller
from include.Middleware.Middleware import Middleware
from include.NetworkWatch.NetworkWatch import NetworkWatcher

if len(sys.argv) == 3:
	# addr = 'localhost'
	# port = 9983
	addr = str(sys.argv[1])
	port = int(sys.argv[2])
	controller = Controller('helios')
	network_watcher = NetworkWatcher(addr, port)

	middleware = Middleware(network_watcher, controller)

	network_watcher.serve_forever()
	middleware_thread = Thread(target=middleware.begin)
	controller_thread = Thread(target=controller.handle_trigger)
	middleware_thread.start()
	controller_thread.start()
	middleware_thread.join()
	controller_thread.join()
	network_watcher.terminate()
else:
	print "Correct usage: " + sys.argv[0] + " <address> <port>"
print "Main thread about to exit"
