import SocketServer


class ThreadedTCPRequestHandler(SocketServer.BaseRequestHandler):
	def handle(self):
		raise NotImplementedError("Must implement a TCP request handler!")


class ThreadedTCPServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
	allow_reuse_address = True
