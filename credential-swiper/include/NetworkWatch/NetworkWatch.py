import threading
from multiprocessing import Queue

from Server import Server


class NetworkWatcher(object):
	class CustomTCPHandler(Server.ThreadedTCPRequestHandler):
		cmd_queue = None  # Outgoing messages
		ack_queue = None  # Incoming messages

		def handle(self):
			# self.request is the TCP socket connected to the client
			data = self.request.recv(1024).strip()
			print "{} wrote on thread {}:".format(self.client_address[0], threading.current_thread().name), data

			if self.cmd_queue is not None:
				try:
					self.cmd_queue.put(data)
				# ack = self.ack_queue.get()
				# assert ack is not None  # TODO make a better assertion.
				except:
					raise

			# just send back the same data, but upper-cased
			self.request.sendall(data.upper())

	def __init__(self, ip_address, ip_port):
		self.cmd_queue = Queue()
		self.ack_queue = Queue()
		NetworkWatcher.CustomTCPHandler.cmd_queue = self.cmd_queue
		NetworkWatcher.CustomTCPHandler.ack_queue = self.ack_queue
		# TODO Make this an HTTP Server
		self.server = Server.ThreadedTCPServer((ip_address, ip_port), NetworkWatcher.CustomTCPHandler)
		# Start a thread with the server -- that thread will then start one
		# more thread for each request
		self.server_thread = threading.Thread(target=self.server.serve_forever)
		print "NetworkWatcher initialized"

	# Exit the server thread when the main thread terminates
	# self.server_thread.daemon = True

	def serve_forever(self):
		self.server_thread.start()
		print "Server loop running in thread:", self.server_thread.name

	def terminate(self):
		self.server.shutdown()
		self.server_thread.join()
		self.server.server_close()
