import socket
import time


def client(ip, port, message):
	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	sock.connect((ip, port))
	try:
		sock.sendall(message)
		response = sock.recv(1024)
		print "Received: {}".format(response)
	finally:
		sock.close()


if __name__ == "__main__":
	addr, port = 'localhost', 9983
	interval = 5
	# client(addr, port, 'f_square@helios:mode=change1')
	# time.sleep(interval)
	# client(addr, port, 'f_square@helios:mode=change2')
	# time.sleep(interval)
	# client(addr, port, 'f_square@helios:mode=change3,style=style3')
	# time.sleep(interval)
	# client(addr, port, 'helios:mode=shutdown')
	# client(addr, port, 'f_square@helios:mode=change1')
	client(
		addr, port,
		# 'ion@helios:mode=logout;timeout=2,is_persistent=True,retry_interval=3,retry_count=10,tolerance_count=5')
		'ion@helios:mode=logout --timeout=2 --is_persistent=True --retry_interval=1 --retry_count=2 --tolerance_count=5')
	time.sleep(interval)
	client(addr, port, 'ion@helios:mode=stop')
	time.sleep(interval)
	# client(addr, port, 'f_square@helios:mode=change2,style=style2')
	client(
		addr, port,
		# 'ion@helios:mode=login,username=140905378,password=rajadgr8 --is_persistent=True --retry_interval=2')
		'ion@helios:mode=login,username=140905378,password=rajadgr8 --retry_interval=5')
	time.sleep(20)
	client(addr, port, 'helios:mode=shutdown')
