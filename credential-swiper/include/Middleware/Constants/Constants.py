class Modes:
	class System:
		SHUTDOWN = "shutdown"

	class Executable:
		DEFAULT = "default"
		STOP = "stop"
		STANDBY = "standby"

	class Queue:
		STOP = "stopq"


class Timeouts:
	NOTDEFINED = -1
	class Workers:
		PERIOD = 10


class ReturnCodes:
	class Executable:
		SUCCESS = 1
		FAIL = 2
