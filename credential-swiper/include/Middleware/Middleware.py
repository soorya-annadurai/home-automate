import re
from multiprocessing import Process, Event, Semaphore, Lock
from threading import Thread

from Constants.Constants import Modes


class Command:
	def __init__(self, raw_string):
		# Expected format: task@controller:mode=<mode>,kwarg1=<kwarg1>
		# Expected format: task@controller:mode=<mode>,kwarg1=<kwarg1>;timeout=<float>
		# TODO Use format: task@controller:mode=<mode>,kwarg1=<kwarg1> --timeout=<float> --is_persistent=<bool> (--etc)
		# TODO Add a parameter for is_interruptable
		self.task_name = None
		self.controller_name = None
		self.kwargs = {}
		self.worker_params = {}
		p = re.compile('[a-zA-Z0-9_]+@')
		m = p.search(raw_string)
		if m is not None:
			self.task_name = m.group().rstrip("@")
		p = re.compile('[a-zA-Z0-9_]+:')
		m = p.search(raw_string)
		if m is not None:
			self.controller_name = m.group().rstrip(":")
		p = re.compile(':[a-zA-Z0-9_=,]+')
		m = p.search(raw_string)
		if m is not None:
			self.kwargs = dict([kwarg.split("=") for kwarg in m.group().lstrip(":").split(",")])
		p = re.compile('(\-\-[a-zA-Z0-9_]+[=][a-zA-Z0-9_]+)+')
		m = p.findall(raw_string)
		if m is not None:
			self.worker_params = dict([worker_param.lstrip("-").split("=") for worker_param in m])

	@classmethod
	def from_mode(cls, task_name, controller_name, mode_name):
		if task_name is not None:
			raw_cmd_str = task_name + "@" + controller_name + ":mode=" + mode_name
		else:
			raw_cmd_str = controller_name + ":mode=" + mode_name
		return cls(raw_cmd_str)


class Middleware(object):
	def __init__(self, network_watcher, controller):
		self.network_watcher = network_watcher
		self.controller = controller
		self.queue_network_incoming = network_watcher.cmd_queue
		self.queue_network_outgoing = network_watcher.ack_queue
		self.queue_controller_incoming = controller.notify_queue
		self.queue_controller_outgoing = controller.trigger_queue
		self._middleware_run_event = Event()
		self._middleware_run_event.set()
		print "Middleware initialized"

	class NetworkTriggerMonitor(Thread):
		def __init__(self, lock, run_state, queue_incoming, queue_outgoing, controller_name):
			Thread.__init__(self)
			self.lock = lock
			self.run_state = run_state
			self.incoming = queue_incoming
			self.outgoing = queue_outgoing
			self.controller_name = controller_name
			self.process = Process(target=self.forward_from_network_to_controller)
			self.kill_ready = Semaphore(0)

		def forward_from_network_to_controller(self):
			while self.run_state.is_set():
				item = self.incoming.get()
				command = Command(item)
				if command.controller_name is not None:
					if command.controller_name == self.controller_name:
						if command.task_name is None:
							# Root mode
							if 'mode' in command.kwargs.keys() and command.kwargs['mode'] == Modes.System.SHUTDOWN:
								self.run_state.clear()
						else:
							self.outgoing.put(command)

		def run(self):
			self.process.start()
			self.kill_ready.release()
			self.process.join()
			try:
				self.lock.release()
			except ValueError:
				pass

		def stop(self):
			self.kill_ready.acquire()
			if self.process is not None and self.process.is_alive():
				self.process.terminate()
			self.process = None

	# TODO Make ControllerTriggerMonitor inherit a shared class for both monitors.
	class ControllerTriggerMonitor(Thread):
		def __init__(self, lock, run_state, queue_incoming, queue_outgoing):
			Thread.__init__(self)
			self.lock = lock
			self.run_state = run_state
			self.incoming = queue_incoming
			self.outgoing = queue_outgoing
			self.process = Process(target=self.forward_from_controller_to_network)
			self.kill_ready = Semaphore(0)

		def forward_from_controller_to_network(self):
			while self.run_state.is_set():
				item = self.incoming.get()
				if item == Modes.System.SHUTDOWN:
					self.run_state.clear()
				else:
					# TODO Change this to an Acknowledgement object
					self.outgoing.put(item)

		def run(self):
			self.process.start()
			self.kill_ready.release()
			self.process.join()
			try:
				self.lock.release()
			except ValueError:
				pass

		def stop(self):
			self.kill_ready.acquire()
			if self.process is not None and self.process.is_alive():
				self.process.terminate()
			self.process = None

	def initiate_system_shutdown(self):
		shutdown_command = Command.from_mode(None, self.controller.controller_name, Modes.System.SHUTDOWN)
		self.queue_controller_outgoing.put(shutdown_command)
		# TODO Use a queue-based class of constants? Or even a command?
		self.queue_network_outgoing.put(Modes.System.SHUTDOWN)
		self._middleware_run_event.clear()

	def begin(self):
		lock = Lock()
		network_trigger_monitor = Middleware.NetworkTriggerMonitor(
			lock,
			self._middleware_run_event,
			self.queue_network_incoming,
			self.queue_controller_outgoing,
			self.controller.controller_name
		)
		controller_trigger_monitor = Middleware.ControllerTriggerMonitor(
			lock,
			self._middleware_run_event,
			self.queue_controller_incoming,
			self.queue_network_outgoing
		)
		lock.acquire()
		network_trigger_monitor.start()
		controller_trigger_monitor.start()

		lock.acquire()
		if network_trigger_monitor.is_alive():
			network_trigger_monitor.stop()
		if controller_trigger_monitor.is_alive():
			controller_trigger_monitor.stop()

		self.initiate_system_shutdown()
		print "Middleware initiated system shutdown"
