import multiprocessing
from multiprocessing import Process, Queue, Event, Lock, Semaphore
from threading import Thread

from include.Middleware.Constants.Constants import Modes, ReturnCodes
from include.utils.EventAndSemaphoreWaitMonitor import EventAndSemaphoreWaitMonitor
from include.utils.MultiProcessQueueWithPutEvent import MultiProcessQueueWithPutEvent


class Persistence:
	class Default:
		is_persistent = False
		retry_interval = -1
		retry_count = 1
		tolerance_count = 1

	def __init__(
			self,
			is_persistent=Default.is_persistent, retry_interval=Default.retry_interval,
			retry_count=Default.retry_count, tolerance_count=Default.tolerance_count):
		assert (is_persistent is False) or (retry_interval >= 0)
		self.is_persistent = is_persistent
		self.retry_interval = retry_interval
		self.retry_count = retry_count
		self.tolerance_count = tolerance_count

	@classmethod
	def from_persistence(cls, persistence):
		return cls(
			persistence.is_persistent,
			persistence.retry_interval,
			persistence.retry_count,
			persistence.tolerance_count)

	@classmethod
	def from_worker_params(cls, worker_params, workermethod):
		if workermethod is None:
			is_persistent = worker_params.pop('is_persistent', Persistence.Default.is_persistent)
			if is_persistent == 'False':
				is_persistent = False
			retry_interval = float(worker_params.pop('retry_interval', Persistence.Default.retry_interval))
			retry_count = int(worker_params.pop('retry_count', Persistence.Default.retry_count))
			tolerance_count = int(worker_params.pop('tolerance_count', Persistence.Default.tolerance_count))
		else:
			is_persistent = worker_params.pop('is_persistent', workermethod.persistence.is_persistent)
			if is_persistent == 'False':
				is_persistent = False
			retry_interval = float(worker_params.pop('retry_interval', workermethod.persistence.retry_interval))
			retry_count = int(worker_params.pop('retry_count', workermethod.persistence.retry_count))
			tolerance_count = int(worker_params.pop('tolerance_count', workermethod.persistence.tolerance_count))
		return cls(is_persistent, retry_interval, retry_count, tolerance_count)


class Task:
	def __init__(self, name, func, args=None, kwargs=None, timeout=0, persistence=Persistence(False, -1, 1)):
		self.name = name
		self.func = func
		self.args = args
		self.kwargs = kwargs
		self.timeout = float(timeout)
		self.persistence = Persistence.from_persistence(persistence)

	@classmethod
	def from_task(cls, task):
		return cls(task.name, task.func, task.args, task.kwargs, task.timeout, task.persistence)


class Executable(Thread):
	def __init__(self, task, lock):
		Thread.__init__(self)
		self.task = task
		# self.task.kwargs['mode'] = mode
		self.mode = self.task.kwargs['mode']
		# print "Mode of task " + task.name + ": " + mode
		self.lock = lock
		self.kill_ready = Semaphore(0)
		self.executable_return_code = None
		self.process = Process(target=self.execute_task)

	def execute_task(self):
		return_code = None
		try:
			if self.task.kwargs is not None:
				if self.task.args is not None:
					self.task.func(*self.task.args, **self.task.kwargs)
				else:
					self.task.func(**self.task.kwargs)
			else:
				if self.task.args is not None:
					self.task.func(*self.task.args)
				else:
					self.task.func()
			return_code = ReturnCodes.Executable.SUCCESS
		except Exception, e:
			print "Caught exception from task <" + self.task.name + ">: " + e.message
			return_code = ReturnCodes.Executable.FAIL
		exit(return_code)

	def run(self):
		try:
			self.process.start()
			self.kill_ready.release()
		except Exception, e:
			print "Couldn't start the process with task " + self.task.name + ": " + str(e)
		finally:
			try:
				if self.task.timeout > 0 and self.process is not None:
					self.process.join(self.task.timeout)
					if self.process is not None and self.process.is_alive():
						self.process.terminate()
				else:
					self.process.join()
			except Exception, e:
				print "SubException: " + str(e) + "..." + str(type(self.task.timeout))
		try:
			self.executable_return_code = self.process.exitcode
			self.lock.release()
		except ValueError:
			pass
		except Exception, e:
			print "Unknown exception from executable " + self.task.name + ": " + str(e)
		pass

	def stop(self):
		print "Stopping executable <" + self.task.name + ">"
		self.kill_ready.acquire()
		if self.process is not None and self.process.is_alive():
			self.process.terminate()
		# self.process = None


class StoppableQueueMonitor(Thread):
	def __init__(self, queue, lock, task):
		Thread.__init__(self)
		self.lock = lock
		self.task = task
		self.queue = queue
		self.extraction_queue = None
		self.process = Process(target=self.queue_extractor)
		self.kill_ready = Semaphore(0)
		self.extracted_command = None
		self._trigger_extraction_lock = multiprocessing.Lock()
		self.extraction_event = multiprocessing.Event()
		self.extraction_event.clear()
		self._stop_event = Event()
		self._stop_event.clear()

	def queue_extractor(self):
		from multiprocessing.queues import Empty
		try:
			self.queue.put_semaphore.acquire()
			self._trigger_extraction_lock.acquire()
			item = self.queue.get(put_semaphore_auto_acquire=False)
			self.extraction_event.set()
			self.extraction_queue.put(item)
		except Empty:
			self.extraction_queue.put(None)

	def run(self):
		# self.extraction_queue = Queue()
		self.extraction_queue = MultiProcessQueueWithPutEvent()
		self.process.start()
		self.kill_ready.release()
		self.process.join()
		try:
			# while self.extraction_queue.empty() and not self._stop_event.is_set():
			# 	pass
			EventAndSemaphoreWaitMonitor(self._stop_event, self.extraction_queue.put_semaphore)
			if not self.extraction_queue.empty():
				self.extracted_command = self.extraction_queue.get()
				self.extraction_queue.close()
				# self.extraction_queue.join_thread()
				self._trigger_extraction_lock.release()
		except Exception, e:
			print e
		try:
			self.lock.release()
		except ValueError:
			pass
		except Exception, e:
			print e

	def stop(self):
		self._stop_event.set()
		self.kill_ready.acquire()
		self._trigger_extraction_lock.acquire()
		if self.process is not None and self.process.is_alive():
			self.process.terminate()
			self._trigger_extraction_lock.release()
		try:
			self.lock.release()
		except ValueError:
			pass
		except Exception, e:
			print e


class Worker(Thread):
	"""Thread executing tasks from a given tasks queue"""

	def __init__(self, task):
		Thread.__init__(self)
		self.is_running = False
		self.has_run = False
		self.task = task
		self.trigger_queue = MultiProcessQueueWithPutEvent()
		self.notify_queue = Queue()
		self._stop_event = Event()
		self._stop_event.clear()
		# self.daemon = True
		pass

	def run(self):
		self.is_running = True
		self.has_run = True
		persistence_flag = self.task.persistence.is_persistent
		retry_interval = self.task.persistence.retry_interval
		retry_count = self.task.persistence.retry_count
		if 'mode' not in self.task.kwargs.keys():
			self.task.kwargs['mode'] = Modes.Executable.DEFAULT
		tolerance = self.task.persistence.tolerance_count
		self._stop_event.clear()
		if retry_count <= 0 or tolerance <= 0:
			self._stop_event.set()

		while not self._stop_event.is_set():
			lock = Lock()
			process_task_monitor = Executable(self.task, lock)
			process_queue_monitor = StoppableQueueMonitor(self.trigger_queue, lock, self.task)
			lock.acquire()
			process_task_monitor.start()
			process_queue_monitor.start()
			lock.acquire()

			if process_task_monitor.is_alive():
				process_task_monitor.stop()
			if process_queue_monitor.is_alive():
				process_queue_monitor.stop()

			process_task_monitor.join()
			process_queue_monitor.join()

			if process_queue_monitor.extracted_command is not None:
				extracted_command = process_queue_monitor.extracted_command
				self.task.kwargs = extracted_command.kwargs
				# TODO Make 'timeout' a customizable parameter
				if 'timeout' in extracted_command.worker_params.keys():
					self.task.timeout = float(extracted_command.worker_params['timeout'])
				try:
					if 'is_persistent' in extracted_command.worker_params.keys():
						self.task.persistence.is_persistent = extracted_command.worker_params['is_persistent'] == 'True'
						persistence_flag = self.task.persistence.is_persistent
					if 'retry_interval' in extracted_command.worker_params.keys():
						self.task.persistence.retry_interval = float(extracted_command.worker_params['retry_interval'])
						retry_interval = self.task.persistence.retry_interval
					if 'retry_count' in extracted_command.worker_params.keys():
						self.task.persistence.retry_count = int(extracted_command.worker_params['retry_count'])
						retry_count = self.task.persistence.retry_count
					if 'tolerance_count' in extracted_command.worker_params.keys():
						self.task.persistence.tolerance_count = int(extracted_command.worker_params['tolerance_count'])
						tolerance = self.task.persistence.tolerance_count
				except ValueError:
					pass

			else:
				assert process_queue_monitor.extraction_event.is_set() is False

			# TODO Make 'mode' a customizable parameter
			if 'mode' in self.task.kwargs.keys() and self.task.kwargs['mode'] == Modes.Executable.STOP:
				self._stop_event.set()

			if process_task_monitor.executable_return_code == ReturnCodes.Executable.SUCCESS:
				tolerance = int(self.task.persistence.tolerance_count)
			else:
				tolerance -= 1
			if persistence_flag is False:
				retry_count -= 1
			if retry_count <= 0 or tolerance <= 0:
				self._stop_event.set()
			# TODO Check if the task is interruptable
			# if retry_interval > 0 and retry_count > 0 and tolerance > 0 and not self._stop_event.is_set():
			if retry_interval > 0 and retry_count > 0 and tolerance > 0 and not self._stop_event.is_set() and not process_queue_monitor.extraction_event.is_set():
				# self._stop_event.wait(timeout=retry_interval)
				EventAndSemaphoreWaitMonitor(
					self._stop_event, self.trigger_queue.put_semaphore, event_timeout=retry_interval)
		print "Worker about to sleep: <" + self.task.name + ">"
		self.trigger_queue.close()
		self.is_running = False

	def request_stop(self):
		self._stop_event.set()


class WorkerCollection:
	"""Collection of workers working on individual tasks"""

	def __init__(self, tasks):
		self.tasks = tasks
		self.workers = []
		# TODO Don't use the mapping for the dictionary; use a string-to-thread direct mapping instead.
		self.mapping = {}
		counter = 0
		for task in tasks:
			self.workers.append(Worker(Task.from_task(task)))
			self.mapping[task.name] = counter
			counter += 1

	def resurrect_worker(self, command, workermethod):
		try:
			print "Resurrecting worker: <" + command.task_name + ">"
			self.workers[self.mapping[command.task_name]] = Worker(
				Task.from_task(self.tasks[self.mapping[command.task_name]]))
			persistence = Persistence.from_worker_params(command.worker_params, workermethod)
			self.workers[self.mapping[command.task_name]].task.persistence = persistence
			self.workers[self.mapping[command.task_name]].task.kwargs = command.kwargs
			if 'timeout' in command.worker_params.keys():
				self.workers[self.mapping[command.task_name]].task.timeout = float(command.worker_params['timeout'])
			else:
				self.workers[self.mapping[command.task_name]].task.timeout = workermethod.timeout
		except KeyError:
			print "Attempted to resurrect a worker that does not exist!"

	def run_all(self):
		for index in range(len(self.workers)):
			self.workers[index].start()

	def request_stop(self, task_name):
		self.workers[self.mapping[task_name]].request_stop()

	def request_stop_all(self):
		for index in range(len(self.workers)):
			self.workers[index].request_stop()

	def wait_completion_all(self, timeout=None):
		for index in range(len(self.workers)):
			if self.workers[index].is_alive():
				if timeout is None:
					self.workers[index].join()
				else:
					self.workers[index].join(timeout)
