from multiprocessing import Event

import Workers.Workers as Workers
from include.Middleware.Constants.Constants import Modes, Timeouts
from include.utils.MultiProcessQueueWithPutEvent import MultiProcessQueueWithPutEvent
from tasks import FunctionCollection


class Controller(object):
	def __init__(self, controller_name):
		self.controller_name = controller_name
		self.trigger_queue = MultiProcessQueueWithPutEvent()
		self.notify_queue = MultiProcessQueueWithPutEvent()
		self.controller_run_state = Event()
		self.controller_run_state.set()

		x = FunctionCollection()
		functions = sorted(
			# get a list of fields that have the order set
			[
				getattr(x, field) for field in dir(x)
				if hasattr(getattr(x, field), "order")
			],
			# sort them by their order
			key=(lambda field: field.order)
		)
		self.functions = functions
		tasks = []
		for func in functions:
			# print "Executing function with name: " + func.name + ":" + str(func)
			tasks.append(Workers.Task(func.name, func, func.args, func.kwargs, func.timeout, func.persistence))
		self.worker_collection = Workers.WorkerCollection(tasks)
		print "Controller initialized"

	def run_all_workers(self):
		self.worker_collection.run_all()

	def handle_trigger(self):
		print "Starting trigger queue handler"
		while self.controller_run_state.is_set():
			command = self.trigger_queue.get()
			if command.task_name is None:
				if 'mode' in command.kwargs.keys() and command.kwargs['mode'] == Modes.System.SHUTDOWN:
					self.controller_run_state.clear()
			else:
				try:
					if not self.worker_collection.workers[
						self.worker_collection.mapping[command.task_name]].is_running:
						if self.worker_collection.workers[self.worker_collection.mapping[command.task_name]].has_run:
							self.worker_collection.workers[self.worker_collection.mapping[command.task_name]].join()
						required_function = None
						for func in self.functions:
							if func.name == command.task_name:
								required_function = func
						self.worker_collection.resurrect_worker(command, required_function)
						self.worker_collection.workers[self.worker_collection.mapping[command.task_name]].start()
					# TODO If the worker isn't working, send a BADACK
					else:
						self.worker_collection.workers[
							self.worker_collection.mapping[command.task_name]].trigger_queue.put(command)
				# print "--- Put the item"
				# TODO Get acknowledgement of command reception and completion from worker.
				except KeyError:
					# TODO Return WorkerNotFound error
					print "--- Worker not found"
					pass
		print "Requesting worker collection to stop"
		self.worker_collection.request_stop_all()
		print "Waiting for worker collection to complete"
		self.worker_collection.wait_completion_all(Timeouts.Workers.PERIOD)
		print "Ending trigger queue handler"
