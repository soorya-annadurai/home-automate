from multiprocessing import Lock
from threading import Thread


class EventAndSemaphoreWaitMonitor:
	"""This expects multiprocessing.Event objects"""

	def wait_for_event(self, event, timeout=None):
		event.wait(timeout)
		try:
			self.lock.release()
		except ValueError:
			pass

	def wait_for_semaphore(self, semaphore, timeout=None, undo_acquire=True):
		if timeout is None:
			semaphore.acquire()
		else:
			semaphore.acquire(int(timeout) + 1)
		if undo_acquire is True:
			semaphore.release()
		try:
			self.lock.release()
		except ValueError:
			pass

	def __init__(self, event, semaphore, event_timeout=None, semaphore_timeout=None, undo_semaphore_acquire=True):
		self.lock = Lock()
		t1 = Thread(target=self.wait_for_event, args=[event, event_timeout])
		t2 = Thread(target=self.wait_for_semaphore, args=[semaphore, semaphore_timeout, undo_semaphore_acquire])
		t1.daemon = True
		t2.daemon = True
		self.lock.acquire()
		t1.start()
		t2.start()
		self.lock.acquire()
