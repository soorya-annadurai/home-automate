import multiprocessing
from multiprocessing.queues import Empty


class MultiProcessQueueWithPutEvent(object):
	# TODO Use an is-a relationship, not has-a relationship for multiprocessing.Queue()
	def __init__(self):
		self.queue = multiprocessing.Queue()
		self.put_semaphore = multiprocessing.Semaphore(value=0)

	def put(self, item):
		self.queue.put(item)
		self.put_semaphore.release()

	def get(self, block=True, timeout=None, put_semaphore_auto_acquire=True):
		item = self.queue.get(block=block, timeout=timeout)
		if put_semaphore_auto_acquire:
			self.put_semaphore.acquire()
		return item

	def qsize(self):
		return self.queue.qsize()

	def empty(self):
		return self.queue.empty()

	def flush(self):
		while not self.queue.empty():
			try:
				self.get()
			except Empty:
				break

	def close(self):
		self.flush()
		self.queue.close()
		self.queue.join_thread()
