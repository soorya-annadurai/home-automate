from __future__ import print_function

import ssl
import sys
import time

import mechanize

import utils


def login(username, password):
	# Handle target environment that doesn't support HTTPS verification
	ssl._create_default_https_context = ssl._create_unverified_context
	br = mechanize.Browser()
	br.set_handle_robots(False)
	response = br.open(utils.ION_URL)

	cleaned_response = utils.clean_response(br, response)
	br.set_response(cleaned_response)

	br.select_form('clientloginform')
	br.form.set_all_readonly(False)
	br.form['username'] = username
	br.form['password'] = password
	br.submit()

	login_response = br.response().read().lower()
	if login_response.find("different user name") != -1:
		raise utils.MultiuserError(time.ctime(), login_response)
	elif login_response.find("wrong") != -1:
		raise utils.CredentialError(time.ctime(), login_response)
	elif login_response.find("used up your allotted") != -1:
		raise utils.QuotaError(time.ctime(), login_response)
	elif login_response.find("successfully") != -1:
		print("Successful login!", end=" ")
	else:
		# This doesn't indicate a bad login. There's no response (green) message.
		# print("Strange scenario.", end=" ")
		# print(login_response)
		raise utils.StrangeError(time.ctime(), login_response)
	print("Timestamp: %s" % time.ctime())


if __name__ == "__main__":
	if len(sys.argv) != 3:
		print("Correct usage: " + sys.argv[0] + " <username> <password>")
	else:
		login(sys.argv[1], sys.argv[2])
