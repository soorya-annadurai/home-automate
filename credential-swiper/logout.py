import ssl
import sys
import time

import mechanize

import utils


def logout():
	ssl._create_default_https_context = ssl._create_unverified_context
	br = mechanize.Browser()
	br.set_handle_robots(False)

	br.open(utils.ION_URL)
	cleaned_response = utils.clean_response(br, br.response())

	br.set_response(cleaned_response)

	br.select_form('clientloginform')
	br.form.set_all_readonly(False)

	br["mode"] = "193"
	br["checkClose"] = "1"

	br.submit()
	mystr = br.response().read().lower()

	if mystr.find("different user name") != -1:
		print("Couldn't log out.")
	else:
		print("Successfully logged out: " + time.ctime())


if __name__ == "__main__":
	if len(sys.argv) != 1:
		print("Correct usage: " + sys.argv[0])
	else:
		logout()
