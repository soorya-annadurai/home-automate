import csv
import sys

CARD_PATH = "card.csv"
CARD_HEADERS = {'username': "USERNAME", 'password': "PASSWORD", 'interval': "LOGIN_RETRY_INTERVAL"}
DEFAULT_INTERVAL = 5


def update_card(username, password, interval, card_path, card_headers):
	new_entry = str(username + "," + password + "," + str(interval)).split(",")
	try:
		with open(card_path, "r"):
			# Test if the file exists.
			pass
		with open(card_path, "ab") as csv_file:
			csv.writer(csv_file, delimiter=',').writerow(new_entry)
		print("Card updated!")
	except IOError:
		data = [
			str(card_headers['username'] + "," + card_headers['password'] + "," + card_headers['interval']).split(","),
			new_entry
		]
		with open(card_path, "wb") as csv_file:
			writer = csv.writer(csv_file, delimiter=',')
			for line in data:
				writer.writerow(line)
		print("Card created!")


if __name__ == "__main__":
	if len(sys.argv) == 3:
		update_card(
			username=sys.argv[1],
			password=sys.argv[2],
			interval=DEFAULT_INTERVAL,
			card_path=CARD_PATH,
			card_headers=CARD_HEADERS
		)
	elif len(sys.argv) == 4:
		update_card(
			username=sys.argv[1],
			password=sys.argv[2],
			interval=sys.argv[3],
			card_path=CARD_PATH,
			card_headers=CARD_HEADERS
		)
	else:
		print(
			"Correct usage: " + "\n" + sys.argv[0] + " <new_username> <new_password> [<login_retry_interval>]")
