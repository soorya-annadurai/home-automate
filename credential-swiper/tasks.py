from driver import is_internet_connected
from include.Controller.Workers.Workers import Persistence
from include.Middleware.Constants.Constants import Modes
from login import login
from logout import logout


def workermethod(name, args=None, kwargs=None, order=66, timeout=0, persistence=Persistence(False, -1, 1, 1)):
	"""max_duration can be negative or zero to disable a timeout"""

	if args is None:
		args = []
	if kwargs is None:
		kwargs = {}

	def do_assignment(to_func):
		to_func.name = name
		to_func.args = args
		to_func.kwargs = kwargs
		to_func.order = order
		to_func.timeout = timeout
		to_func.persistence = persistence
		return staticmethod(to_func)

	return do_assignment


# noinspection PyClassHasNoInit,PyMethodParameters,PyTypeChecker
class FunctionCollection:
	# @workermethod(name='f_bar', kwargs={'mode': Modes.Executable.DEFAULT})
	# @workermethod(name='f_bar', kwargs={'mode': None})
	# def bar(**kwargs):
	# 	mode = kwargs.pop('mode', None)
	# 	if mode is None:
	# 		raise AttributeError("Mode of operation is not defined.")
	# 	ans = "bar"
	# 	print ans
	# 	print "Mode:" + mode
	# 	return

	# @workermethod(name='f_foo')
	# def foo(mode):
	# 	ans = "foo"
	# 	print ans
	# 	print "Mode:" + mode
	# 	return

	@workermethod(
		name='f_square', args=['3'], kwargs={'mode': 'customMode'}, timeout=4,
		persistence=Persistence(is_persistent=True, retry_interval=1, retry_count=4, tolerance_count=2))
	def sq(n, **kwargs):
		mode = kwargs.pop('mode', None)
		if mode is None:
			raise AttributeError("Mode of operation is not defined.")
		ans = str(int(n) * int(n))
		# sleep(0.5)
		print str(n) + " * " + str(n) + " = " + str(ans)
		# print "**********Mode:" + mode
		# raise AssertionError("Sample assertion error")
		return

	# @workermethod(name='f_cube', args=['2'])
	# def cu(n, mode):
	# 	ans = str(int(n) * int(n) * int(n))
	# 	print ans
	# 	print "Mode:" + mode
	# 	return
	#

	@workermethod(
		name='ion', timeout=10,
		persistence=Persistence(is_persistent=True, retry_interval=60, tolerance_count=3))
	def credential_swiper(**kwargs):
		print "Entering credential swiper with kwargs: " + str(kwargs)
		mode = kwargs.pop('mode', None)
		if mode is None:
			raise AttributeError("Check the mode that was passed in.")
		if mode == Modes.Executable.DEFAULT:
			return
		elif mode == 'login':
			username = kwargs.pop('username', None)
			if username is None:
				raise AttributeError("Check the username that was passed in.")
			password = kwargs.pop('password', None)
			if password is None:
				raise AttributeError("Check the password that was passed in.")
			if not is_internet_connected():
				login(username, password)
				print "Logged in!"
			else:
				print "Already connected to internet!"
		elif mode == 'logout':
			logout()
			print "Logged out!"
